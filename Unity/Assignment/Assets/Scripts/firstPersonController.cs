﻿using UnityEngine;
using System.Collections;

public class firstPersonController : MonoBehaviour {

	public float movementSpeed = 5.0f;
	public float mouseSensitivty = 5.0f;
	public float verticalRange = 60.0f;
	public float rotXaxis = 0;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float rotYaxis = Input.GetAxis ("Mouse X") * mouseSensitivty;
		transform.Rotate (0, rotYaxis, 0);
		rotXaxis -= Input.GetAxis ("Mouse Y") * mouseSensitivty;
		rotXaxis = Mathf.Clamp (rotXaxis, -verticalRange, verticalRange);
		Camera.main.transform.localRotation = Quaternion.Euler (rotXaxis, 0, 0);
		float fSpeed = Input.GetAxis("Vertical") * movementSpeed ;
		float hSpeed = Input.GetAxis("Horizontal") * movementSpeed ;
		Vector3 speed = new Vector3 (hSpeed, 0, fSpeed);
		speed = transform.rotation * speed; //new code
			CharacterController cc = GetComponent<CharacterController>();

		cc.SimpleMove (speed);

	}
}
