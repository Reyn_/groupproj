﻿using UnityEngine;
using System.Collections;

public class DiamondControllerSound : MonoBehaviour {
	AudioSource audio;
	// Use this for initialization
	
	void Start() {
		audio = GetComponent<AudioSource>();
	}
	
	void OnTriggerEnter(Collider col ){
		if(col.gameObject.tag == "Player"){
			audio.Play();
		}
	}
}
